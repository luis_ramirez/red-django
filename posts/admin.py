from django.contrib import admin
from .models import Post

@admin.register(Post)
class PostsAdmin(admin.ModelAdmin):
    list_display = ('title', 'picture', 'profile', 'user')
    search_fields = (
        'user__email',
        'user__username',
        'title'
    )
    list_filter = (
        'created',
        'modified'
    )
    