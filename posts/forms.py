from django.forms import ModelForm
from posts.models import Post

class PostForm(ModelForm):
    class Meta:
        # Form settings
        model = Post
        fields = ('user', 'profile', 'title', 'picture')