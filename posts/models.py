from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Post(models.Model):
    # Foreign keys to make reference to the proper user
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    profile = models.ForeignKey('users.Profile', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    picture = models.ImageField(upload_to='posts/profile')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} by: {}'.format(self.title, self.user.username)