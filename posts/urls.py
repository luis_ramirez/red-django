from django.urls import path
from posts.views import (PostsFeedView, CreatePostView, PostDetailView)

urlpatterns = [
    path(
        route='new', 
        view=CreatePostView.as_view(),
        name='create_post'
    ),
    path(
        route='posts', 
        view=PostsFeedView.as_view(),
        name='feed'
    ),
    
]

"""path(
    route='<str:post_id>',
    view=PostDetailView.as_view(),
    name='post_detail'
),"""