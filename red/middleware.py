
from django.shortcuts import redirect
from django.urls import reverse
class ProfileCompletionMiddleware:
    """
    Ensures every user interacting with the plattform
    have their profile completed
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Snipet excecuted for every request to the view
        if not request.user.is_anonymous:
            if not request.user.is_staff:
                profile = request.user.profile
                if not profile.picture or not profile.biography:
                    if request.path not in [reverse('users:user_update'), reverse('users:logout')]:
                        return redirect('users:user_update')
        return self.get_response(request)

