# Base django imports
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# Models
from users.models import Profile
from django.contrib.auth.models import User


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    # Elements of model to be rendered in the admin panel
    list_display = ('pk', 'user', 'phone_number', 'website', 'facebook', 'picture')
    list_display_links = ('pk', 'user')
    list_editable = ('phone_number', 'picture')

    search_fields = (
        'user__email',
        'user__username',
        'user__first_name',
        'user__last_name',
        'phone_number'
    )

    list_filter = (
        'created',
        'modified',
        'user__is_active',
        'user__is_staff'
    )

    fieldsets = (
        ('Profile', {
            'fields': (
                ('user', 'picture'),
            )
        }),
        ('Extra info', {
            'fields': (
                ('website', 'phone_number'),
                ('biography')
            )
        }),
        ('Metadata', {
            'fields': ('created', 'modified')
        })
    )

    readonly_fields = ('created', 'modified')

class ProfileInline(admin.StackedInline):
    """Profile in line admin for users"""
    model = Profile
    can_delete = False
    verbose_name_plural = 'profiles'

class UserAdmin(BaseUserAdmin):
    """Adds tjhe profile admin to the base user admin"""
    inlines = (ProfileInline, )
    list_display = ('username', 'email', 'first_name', 'last_name',
        'is_active', 'is_staff')

# Unregisters the built-in User model from the admin
admin.site.unregister(User)
"""
Registers the model and class to be used as the Admin user
this will unite as one form the register admin and profile
previously separated as two diferent models
"""
admin.site.register(User, UserAdmin)