from django import forms
from django.contrib.auth.models import User
from users.models import Profile

class SignupForm(forms.Form):
    username = forms.CharField(
        label=False,
        min_length=3,
        max_length=200,
        required=True,
        widget = forms.TextInput(
            attrs={
                'placeholder':'Nombre de usuario',
                'class': 'form-control',
                'required': True
            }
        )
    )
    password = forms.CharField(
        label=False,
        min_length=3,
        max_length=60,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Contraseña',
                'class': 'form-control',
                'required': True
            }
        )
    )
    password_confirm = forms.CharField(
        label=False,
        min_length=3,
        max_length=50,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Confirmar contraseña',
                'class': 'form-control',
                'required': True
            }
        )
    )
    first_name = forms.CharField(
        label=False,
        min_length=2, 
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'placeholder':'Nombre(s)',
                'class': 'form-control',
                'required': True
            }
        )
    )
    last_name = forms.CharField(
        label=False,
        min_length=2,
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'placeholder':'Apellido(s)',
                'class': 'form-control',
                'required': True
            }
        )
    )
    email = forms.CharField(
        label=False,
        min_length=6,
        max_length=70,
        widget=forms.EmailInput(
            attrs={
                'placeholder':'Correo',
                'class': 'form-control',
                'required': True
            }
        )
    )

    def clean_username(self):
        # Validates username to be unique
        username = self.cleaned_data.get('username')
        username_taken = User.objects.filter(username=username).exists()
        if username_taken:
            raise forms.ValidationError('Nombre de usario ya existente')
        return username

    def clean(self):
        # Last step in form verification
        data = super().clean()
        password = data.get('password')
        password_confirm = data.get('password_confirm')
        if password != password_confirm:
            raise forms.ValidationError('Las contraseñas no coinciden')
        return data

    def save(self):
        data = self.cleaned_data
        data.pop('password_confirm')
        user = User.objects.create_user(**data)
        profile = Profile(user=user)
        profile.save()