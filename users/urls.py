from django.urls import path
from users.views import (LoginView, LogoutView,
    UpdateProfileView, UserDetailView,
    SignupView)


urlpatterns = [
    path(
        route='login',
        view=LoginView.as_view(),
        name='login'
    ),
    path(
        route='logout',
        view=LogoutView.as_view(),
        name='logout'
    ),
    path(
        route='signup',
        view=SignupView.as_view(),
        name='signup'
    ),
    path(
        route='mw/update',
        view=UpdateProfileView.as_view(),
        name='user_update'
    ),
    path(
        route='profile/<str:username>',
        view=UserDetailView.as_view(),
        name='detail'
    ),
]